module.exports = {
    title: '老陈笔记',
    description: '记录前端笔记',
    theme: 'vdoing',
    head: [
        ['link',
            { rel: 'icon', href: '/favicon.ico' }

        ],
    ],
    themeConfig: {
        logo: '/favicon.ico',  //网页顶端导航栏左上角的图标

        //顶部导航栏
        nav: [
            //格式一：直接跳转，'/'为不添加路由，跳转至首页
            { text: '首页', link: '/'},
            //格式二：添加下拉菜单，link指向的文件路径
            {
                text: '学习',  //默认显示
                ariaLabel: '学习',   //用于识别的label
                items: [
                    { text: 'HTML', link: '/pages/HTML/html.md' },
                    { text: 'CSS', link: '/pages/CSS/css2.md' },
                    { text: 'JavaScript', link: '/pages/JavaScript/js.md' },
                    { text: '前端八股文', link: '/pages/Expand/expand1.md' },
                ]
            },
            {
                text: '资源列表',  //默认显示
                ariaLabel: '资源列表',   //用于识别的label
                items: [
                { text: 'ES6', link: 'https://wangdoc.com/es6/intro.html' },
                { text: 'Vue', link: 'https://v2.cn.vuejs.org/v2/guide' },
                { text: 'React', link: 'https://react.docschina.org/tutorial/tutorial.html' },
                { text: 'uniapp', link: 'https://uniapp.dcloud.net.cn' },
                { text: 'ReactNative', link: 'https://www.reactnative.cn/docs/getting-started' },
                { text: '印记中文', link: 'https://docschina.org/' },
                { text: '菜鸟教程', link: 'https://www.runoob.com/' },
                { text: 'Bootstrap', link: 'https://v3.bootcss.com/getting-started/' },
                { text: 'Element', link: 'https://element.eleme.io/#/zh-CN/component/installation' },
                { text: 'Element-plus', link: 'https://element-plus.gitee.io/zh-CN/guide/design.html' },
                { text: 'uview', link: 'https://www.uviewui.com/components/intro.html' },
                { text: 'BootCDN', link: 'https://www.bootcdn.cn/' },
                { text: 'Axios', link: 'https://www.axios-http.cn/docs/example' },
                { text: 'Node', link: 'http://nodejs.cn/learn' },
                { text: 'Ant', link: 'https://ant.design/docs/react/introduce-cn' },
                { text: 'Flutter', link: 'https://flutter.cn/docs/get-started/install' },
                { text: '阿里图标', link: 'https://www.iconfont.cn/' },
                { text: '微信小程序', link: 'https://developers.weixin.qq.com/miniprogram/introduction' },
                  ]
            },
            {
                text: '人生感悟',  //默认显示
                ariaLabel: '人生感悟',   //用于识别的label
                items: [
                    { text: '活着', link: '/Life/alive.md' },
                    { text: '人生的意义', link: '/Life/meaning_of_life.md' },
                    { text: '人生格言', link: '/Life/lifetime.md' },
                ]
            },
            {
                text: '下载',  //默认显示
                ariaLabel: '下载',   //用于识别的label
                items: [
                    { text: 'app安卓精简版', link: 'https://vkceyugu.cdn.bspapp.com/VKCEYUGU-2b5692dd-39ab-498e-b867-9a220d2bb2f2/cb0012d8-d638-4aee-b6a9-ee70ca38453d.apk' },
                    { text: 'app安卓用户版', link: 'https://vkceyugu.cdn.bspapp.com/VKCEYUGU-2b5692dd-39ab-498e-b867-9a220d2bb2f2/5cc6ae73-c06e-49ae-a7a6-fcccdcc31524.apk' },
                ]
            },
            { text: 'gitee', link: 'https://gitee.com/old-chen-next-door/lao-chens-front-end-notes' },

            //格式三：跳转至外部网页，需http/https前缀
            { text: 'just-relax', link: 'http://chenyanjun.3vhost.net' },
        ],
        //静态侧边导航栏：会根据当前的文件路径是否匹配侧边栏数据，自动显示/隐藏
        sidebar: {
            '/pages/':[
                {
                    title: 'HTML',   // 一级菜单名称
                    sidebarDepth: 2,
                    collapsable: true, // false为默认展开菜单, 默认值true是折叠,
                       //  设置侧边导航自动提取markdown文件标题的层级，默认1为h2层级
                    children: [
                        ['HTML/html.md', 'HTML简介'],
                        ['HTML/html5.md', 'HTML5简介']
                    ]
                },
                {
                    title: 'CSS',
                    sidebarDepth: 2,
                    collapsable: true,
                    children: [
                        ['CSS/css2.md', 'CSS简介'],
                        ['CSS/css3.md', 'CSS3简介']
                    ]
                },
                {
                    title: 'JavaScript',
                    sidebarDepth: 2,
                    collapsable: true,
                    children: [
                        ['JavaScript/js.md', 'JavaScript简介']
                    ]
                },
                {
                    title: '前端八股文',
                    sidebarDepth: 2,
                    collapsable: true,
                    children: [
                        ['Expand/expand1.md', '高频常见'],
                        ['Expand/expand2.md', '基础备用']
                    ]
                }
            ],
            '/Life/':[   {
                title: '人生感悟',
                collapsable:  false,
                children: [
                    ['alive.md', '活着'],
                    ['meaning_of_life.md', '人生的意义'],
                    ['lifetime.md', '人生格言']
                ]
            }]
            //...可添加多个不同的侧边栏，不同页面会根据路径显示不同的侧边栏
        }
    }
//   动态添加侧边导航栏
}
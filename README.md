# 老陈前端笔记

#### 介绍
   个人学习前端的总结笔记，知识点如有有错误的之处请大家帮忙修正，后面会持续更新，希望大佬们能分享一下宝贵的前端知识或者心得供我们学习，谢谢。笔记在线查看请访问：http://www.weblearn.fit/

#### 软件架构
软件架构说明


#### 安装教程
1.  cd mynotes
1.  npm install
2.  npm run dev
3.  运行后默认用浏览器打开：http://localhost:8080/


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
